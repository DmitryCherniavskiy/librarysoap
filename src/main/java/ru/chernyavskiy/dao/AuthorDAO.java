package ru.chernyavskiy.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.chernyavskiy.models.Author;

import java.util.List;
import java.util.Optional;

/**
 * @author Cherniavskiy Dmitry
 */
@Component
public class AuthorDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public AuthorDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Author> index() {
        return jdbcTemplate.query("SELECT * FROM author", new BeanPropertyRowMapper<>(Author.class));
    }

    public Optional<Author> show(Integer id) {
        return Optional.ofNullable(jdbcTemplate.query("SELECT * FROM author WHERE id=?", new BeanPropertyRowMapper<>(Author.class), id)
                .stream().findAny().orElse(null));
    }

    public int save(Author author) {
        return jdbcTemplate.update("INSERT INTO author(name, biography)" +
                        " VALUES(?, ?)", author.getName(), author.getBiography());
    }

    public int update(Integer id, Author updatedAuthor) {
        return jdbcTemplate.update("UPDATE author SET name = ?, biography = ? WHERE id=?",
                updatedAuthor.getName(), updatedAuthor.getBiography(), id);
    }

    public void delete(Integer id) {
        jdbcTemplate.update("DELETE FROM author WHERE id=?", id);
    }
}
