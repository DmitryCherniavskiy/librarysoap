package ru.chernyavskiy.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.chernyavskiy.models.Book;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

/**
 * @author Cherniavskiy Dmitry
 */
@Component
public class BookDAO {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public BookDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Book> index() {
        return jdbcTemplate.query("SELECT * FROM book", new BeanPropertyRowMapper<>(Book.class));
    }

    public Optional<Book> show(Integer id) {
        return Optional.ofNullable(jdbcTemplate.query("SELECT * FROM book WHERE id=?", new BeanPropertyRowMapper<>(Book.class), id)
                .stream().findAny().orElse(null));
    }

    public int save(Book book) {
        return jdbcTemplate.update("INSERT INTO book(name, description, publishing_year, publishing_house, author_id, genre_id)" +
                        " VALUES(?, ?, ?, ?, ?, ?)", book.getName(), book.getDescription(), book.getPublishingYear(), book.getPublishingHouse(),
                        book.getAuthorId(), book.getGenreId());
    }

    public int update(Integer id, Book updatedBook) {
        return jdbcTemplate.update("UPDATE book SET name = ?, description = ?, publishing_year = ?, publishing_house = ?, author_id = ?, genre_id = ? WHERE id=?",
                updatedBook.getName(), updatedBook.getDescription(), updatedBook.getPublishingYear(), updatedBook.getPublishingHouse(),
                updatedBook.getAuthorId(), updatedBook.getGenreId(), id);
    }

    public void delete(Integer id) {
        jdbcTemplate.update("DELETE FROM book WHERE id=?", id);
    }

    public List<Book> findByGenreId(Integer id) {
        return jdbcTemplate.query("SELECT * FROM book WHERE genre_id=?", new BeanPropertyRowMapper<>(Book.class), id);
    }

    public List<Book> findByAuthorId(Integer id) {
        return jdbcTemplate.query("SELECT * FROM book WHERE author_id=?", new BeanPropertyRowMapper<>(Book.class), id);
    }
}
