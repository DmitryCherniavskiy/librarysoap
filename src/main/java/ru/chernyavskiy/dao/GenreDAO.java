package ru.chernyavskiy.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.chernyavskiy.models.Genre;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

/**
 * @author Cherniavskiy Dmitry
 */
@Component
public class GenreDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public GenreDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Genre> index() {
        return jdbcTemplate.query("SELECT * FROM genre", new BeanPropertyRowMapper<>(Genre.class));
    }

    public Optional<Genre> show(Integer id) {
        return Optional.ofNullable(jdbcTemplate.query("SELECT * FROM genre WHERE id=?", new BeanPropertyRowMapper<>(Genre.class), id)
                .stream().findAny().orElse(null));
    }

    public int save(Genre genre) {
        return jdbcTemplate.update("INSERT INTO genre(name) VALUES(?)", genre.getName());
    }

    public int update(Integer id, Genre updatedGenre) {
        return jdbcTemplate.update("UPDATE genre SET name = ? WHERE id=?",
                updatedGenre.getName(), id);
    }

    public void delete(Integer id) {
        jdbcTemplate.update("DELETE FROM genre WHERE id=?", id);
    }
}
