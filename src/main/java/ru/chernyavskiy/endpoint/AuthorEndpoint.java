package ru.chernyavskiy.endpoint;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.chernyavskiy.authorgen.*;
import ru.chernyavskiy.models.Author;
import ru.chernyavskiy.service.AuthorService;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class AuthorEndpoint {
    private static final String NAMESPACE_URI = "http://www.chernyavskiy.ru/authorGen";

    private AuthorService authorService;

    @Autowired
    public AuthorEndpoint(AuthorService authorService) {
        this.authorService = authorService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAuthorByIdRequest")
    @ResponsePayload
    public GetAuthorByIdResponse getAuthorById(@RequestPayload GetAuthorByIdRequest request) {
        GetAuthorByIdResponse response = new GetAuthorByIdResponse();
        Author author = authorService.getAuthorById(request.getAuthorId());
        AuthorType authorType = new AuthorType();
        BeanUtils.copyProperties(author, authorType);
        response.setAuthorType(authorType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllAuthorsRequest")
    @ResponsePayload
    public GetAllAuthorsResponse getAllAuthors(@RequestPayload GetAllAuthorsRequest request) {
        GetAllAuthorsResponse response = new GetAllAuthorsResponse();
        List<AuthorType> authorTypeList = new ArrayList<AuthorType>();
        Iterable<Author> authorList = authorService.getAllAuthor();
        for (Author entity : authorList) {
            AuthorType authorType = new AuthorType();
            BeanUtils.copyProperties(entity, authorType);
            authorTypeList.add(authorType);
        }
        response.getAuthorType().addAll(authorTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addAuthorRequest")
    @ResponsePayload
    public AddAuthorResponse addAuthor(@RequestPayload AddAuthorRequest request) {
        AddAuthorResponse response = new AddAuthorResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        try {
            authorService.save(new Author(request.getName(), request.getBiography()));
        }catch (Exception ex){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Author");
            response.setServiceStatus(serviceStatus);
            return response;
        }

        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Added Successfully");
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateAuthorRequest")
    @ResponsePayload
    public UpdateAuthorResponse updateAuthor(@RequestPayload UpdateAuthorRequest request) {
        UpdateAuthorResponse response = new UpdateAuthorResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        try {
            authorService.getAuthorById(request.getId());
        }catch (Exception ex){
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Author = " + request.getId() + " not found");
            response.setServiceStatus(serviceStatus);
            return response;
        }

        try {
            authorService.editById(request.getId(), new Author(request.getName(), request.getBiography()));
        }catch (Exception ex) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while updating Entity=" + request.getId());
            response.setServiceStatus(serviceStatus);
            return response;
        }
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content updated Successfully");
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteAuthorRequest")
    @ResponsePayload
    public DeleteAuthorResponse deleteAuthor(@RequestPayload DeleteAuthorRequest request) {
        DeleteAuthorResponse response = new DeleteAuthorResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        try{
            authorService.getAuthorById(request.getAuthorId());
        }catch(Exception ex){
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Not found author with id=" + request.getAuthorId());
            response.setServiceStatus(serviceStatus);
            return response;
        }
        try{
            authorService.deleteById(request.getAuthorId());
        }catch (Exception ex){
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while delete Entity id=" + request.getAuthorId());
            response.setServiceStatus(serviceStatus);
            return response;
        }

        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Deleted Successfully");
        response.setServiceStatus(serviceStatus);
        return response;
    }
}
