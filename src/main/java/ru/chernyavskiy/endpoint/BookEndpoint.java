package ru.chernyavskiy.endpoint;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ru.chernyavskiy.bookgen.*;
import ru.chernyavskiy.models.Book;
import ru.chernyavskiy.service.BookService;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class BookEndpoint {
    private static final String NAMESPACE_URI = "http://www.chernyavskiy.ru/bookGen";

    private BookService bookService;

    @Autowired
    public BookEndpoint(BookService bookService) {
        this.bookService = bookService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBookByIdRequest")
    @ResponsePayload
    public GetBookByIdResponse getBookById(@RequestPayload GetBookByIdRequest request) {
        GetBookByIdResponse response = new GetBookByIdResponse();
        Book book = bookService.getBookById(request.getBookId());
        BookType bookType = new BookType();
        BeanUtils.copyProperties(book, bookType);
        response.setBookType(bookType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBookByAuthorIdRequest")
    @ResponsePayload
    public GetBookByAuthorIdResponse getBookByAuthorId(@RequestPayload GetBookByAuthorIdRequest request) {
        GetBookByAuthorIdResponse response = new GetBookByAuthorIdResponse();
        List<Book> booksList = bookService.getBookByAuthorId(request.getAuthorId());
        List<BookType> booksTypeList = new ArrayList<BookType>();
        for (Book entity : booksList) {
            BookType bookType = new BookType();
            BeanUtils.copyProperties(entity, bookType);
            booksTypeList.add(bookType);
        }
        response.getBookType().addAll(booksTypeList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBookByGenreIdRequest")
    @ResponsePayload
    public GetBookByGenreIdResponse getBookByGenreId(@RequestPayload GetBookByGenreIdRequest request) {
        GetBookByGenreIdResponse response = new GetBookByGenreIdResponse();
        List<Book> booksList = bookService.getBookByGenreId(request.getGenreId());
        List<BookType> booksTypeList = new ArrayList<BookType>();
        for (Book entity : booksList) {
            BookType bookType = new BookType();
            BeanUtils.copyProperties(entity, bookType);
            booksTypeList.add(bookType);
        }
        response.getBookType().addAll(booksTypeList);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllBooksRequest")
    @ResponsePayload
    public GetAllBooksResponse getAllBooks(@RequestPayload GetAllBooksRequest request) {
        GetAllBooksResponse response = new GetAllBooksResponse();
        List<BookType> bookTypeList = new ArrayList<BookType>();
        Iterable<Book> bookList = bookService.getAllBook();
        for (Book entity : bookList) {
            BookType bookType = new BookType();
            BeanUtils.copyProperties(entity, bookType);
            bookTypeList.add(bookType);
        }
        response.getBookType().addAll(bookTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addBookRequest")
    @ResponsePayload
    public AddBookResponse addBook(@RequestPayload AddBookRequest request) {
        AddBookResponse response = new AddBookResponse();
        BookType newBookType = new BookType();
        ServiceStatus serviceStatus = new ServiceStatus();
        Book savedBook = new Book(request.getName(), request.getDescription(), request.getPublishingYear(), request.getPublishingHouse(), request.getGenreId(), request.getAuthorId());
        try{
            bookService.save(savedBook);
        }catch(Exception ex){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Book");
            response.setServiceStatus(serviceStatus);
            return response;
        }
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Added Successfully");
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateBookRequest")
    @ResponsePayload
    public UpdateBookResponse updateBook(@RequestPayload UpdateBookRequest request) {
        UpdateBookResponse response = new UpdateBookResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        Book bookFromDB;
        try {
            bookFromDB = bookService.getBookById(request.getId());
        }catch (Exception ex) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Book id = " + request.getId() + " not found");
            response.setServiceStatus(serviceStatus);
            return response;
        }

        bookFromDB.setName(request.getName());
        bookFromDB.setDescription(request.getDescription());
        bookFromDB.setPublishingYear(request.getPublishingYear());
        bookFromDB.setPublishingHouse(request.getPublishingHouse());
        bookFromDB.setGenreId(request.getGenreId());
        bookFromDB.setAuthorId(request.getAuthorId());

        try {
            bookService.editById(request.getId(), bookFromDB);
        }catch (Exception ex){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while updating Entity=" + request.getId());
            response.setServiceStatus(serviceStatus);
            return response;
        }
        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content updated Successfully");
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteBookRequest")
    @ResponsePayload
    public DeleteBookResponse deleteBook(@RequestPayload DeleteBookRequest request) {
        DeleteBookResponse response = new DeleteBookResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        try{
            bookService.getBookById(request.getBookId());
        }catch(Exception ex){
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Not found book with id=" + request.getBookId());
            response.setServiceStatus(serviceStatus);
            return response;
        }

        try {
            bookService.deleteById(request.getBookId());
        }catch (Exception ex){
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getBookId());
            response.setServiceStatus(serviceStatus);
            return response;
        }

        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content deleted successfully");
        response.setServiceStatus(serviceStatus);
        return response;
    }
}
