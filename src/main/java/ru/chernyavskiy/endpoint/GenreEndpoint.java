package ru.chernyavskiy.endpoint;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.chernyavskiy.genregen.*;
import ru.chernyavskiy.models.Genre;
import ru.chernyavskiy.service.GenreService;

import java.util.ArrayList;
import java.util.List;


@Endpoint
public class GenreEndpoint {

    private static final String NAMESPACE_URI = "http://www.chernyavskiy.ru/genreGen";

    private GenreService genreService;

    @Autowired
    public GenreEndpoint(GenreService genreService) {
        this.genreService = genreService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getGenreByIdRequest")
    @ResponsePayload
    public GetGenreByIdResponse getGenreById(@RequestPayload GetGenreByIdRequest request) {
        GetGenreByIdResponse response = new GetGenreByIdResponse();
        Genre genre = genreService.getGenreById(request.getGenreId());
        GenreType genreType = new GenreType();
        BeanUtils.copyProperties(genre, genreType);
        response.setGenreType(genreType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllGenresRequest")
    @ResponsePayload
    public GetAllGenresResponse getAllGenres(@RequestPayload GetAllGenresRequest request) {
        GetAllGenresResponse response = new GetAllGenresResponse();
        List<GenreType> genreTypeList = new ArrayList<GenreType>();
        Iterable<Genre> genreList = genreService.getAllGenre();
        for (Genre entity : genreList) {
            GenreType genreType = new GenreType();
            BeanUtils.copyProperties(entity, genreType);
            genreTypeList.add(genreType);
        }
        response.getGenreType().addAll(genreTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addGenreRequest")
    @ResponsePayload
    public AddGenreResponse addGenre(@RequestPayload AddGenreRequest request) {
        AddGenreResponse response = new AddGenreResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        try {
            genreService.save(new Genre(request.getName()));
        }catch(Exception ex){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding genre");
            response.setServiceStatus(serviceStatus);
            return response;
        }

        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Added Successfully");
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateGenreRequest")
    @ResponsePayload
    public UpdateGenreResponse updateGenre(@RequestPayload UpdateGenreRequest request) {
        UpdateGenreResponse response = new UpdateGenreResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        try{
            genreService.getGenreById(request.getId());
        }catch(Exception ex){
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Genre = " + request.getId() + " not found");
            response.setServiceStatus(serviceStatus);
            return response;
        }


        try{
            genreService.editById(request.getId(), new Genre(request.getName()));
        }catch(Exception ex){
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while updating Entity=" + request.getId());
            response.setServiceStatus(serviceStatus);
            return response;
        }

        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content updated Successfully");

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteGenreRequest")
    @ResponsePayload
    public DeleteGenreResponse deleteGenre(@RequestPayload DeleteGenreRequest request) {
        DeleteGenreResponse response = new DeleteGenreResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        try{
            genreService.getGenreById(request.getGenreId());
        }catch (Exception ex){
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Not found genre with id=" + request.getGenreId());
            response.setServiceStatus(serviceStatus);
            return response;
        }

        try{
            genreService.deleteById(request.getGenreId());
        }catch(Exception ex){
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while delete Entity id=" + request.getGenreId());
            response.setServiceStatus(serviceStatus);
            return response;
        }

        serviceStatus.setStatusCode("SUCCESS");
        serviceStatus.setMessage("Content Deleted Successfully");
        response.setServiceStatus(serviceStatus);
        return response;
    }
}
