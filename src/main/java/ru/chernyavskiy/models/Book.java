package ru.chernyavskiy.models;


/**
 * @author Cherniavskiy Dmitry
 */
public class Book {
    private Integer id;

    private String name;

    private String description;

    private Integer publishingYear;

    private String publishingHouse;

    private Integer genreId;

    private Integer authorId;


    public Book(){

    }

    public Book(String name) {
        this.name = name;
    }

    public Book(Integer id, String name, String description, Integer publishingYear, String publishingHouse, Integer genreId, Integer authorId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.publishingYear = publishingYear;
        this.publishingHouse = publishingHouse;
        this.genreId = genreId;
        this.authorId = authorId;
    }

    public Book(String name, String description, Integer publishingYear, String publishingHouse, Integer genreId, Integer authorId) {
        this.name = name;
        this.description = description;
        this.publishingYear = publishingYear;
        this.publishingHouse = publishingHouse;
        this.genreId = genreId;
        this.authorId = authorId;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPublishingYear() {
        return publishingYear;
    }

    public void setPublishingYear(Integer publishingYear) {
        this.publishingYear = publishingYear;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

}
