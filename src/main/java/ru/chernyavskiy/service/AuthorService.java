package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.dao.AuthorDAO;
import ru.chernyavskiy.models.Author;

import java.util.List;

@Service
public class AuthorService {
    private final AuthorDAO authorDAO;

    @Autowired
    public AuthorService(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }

    public List<Author> getAllAuthor(){
        return authorDAO.index();
    }

    public Author getAuthorById(Integer id){
        return authorDAO.show(id).orElse(null);
    }

    public int save(Author author){
        return authorDAO.save(author);
    }

    public int editById(Integer id, Author author){
        if (authorDAO.show(id) != null){
            return authorDAO.update(id, author);
        }
        return 0;
    }
    public void deleteById(Integer id){
        if (authorDAO.show(id) != null){
            authorDAO.delete(id);
        }
    }

}
