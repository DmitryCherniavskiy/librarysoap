package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.dao.BookDAO;
import ru.chernyavskiy.dao.BookDAO;
import ru.chernyavskiy.dao.GenreDAO;
import ru.chernyavskiy.models.Book;


import java.math.BigInteger;
import java.util.List;

@Service
public class BookService {
    private final BookDAO bookDAO;
    private final GenreDAO genreDAO;
    private final BookDAO authorDAO;

    @Autowired
    public BookService(BookDAO bookDAO, GenreDAO genreDAO, BookDAO authorDAO) {
        this.bookDAO = bookDAO;
        this.genreDAO = genreDAO;
        this.authorDAO = authorDAO;
    }

    public List<Book> getAllBook(){
        List<Book> bookList = bookDAO.index();
        return bookList;
    }

    public Book getBookById(Integer id){
        return bookDAO.show(id).get();
    }

    public List<Book> getBookByGenreId(Integer id){
        return bookDAO.findByGenreId(id);
    }

    public List<Book> getBookByAuthorId(Integer id){
        return bookDAO.findByAuthorId(id);
    }

    public int save(Book book){
        return bookDAO.save(book);
    }

    public int editById(Integer id, Book book){
        if (bookDAO.show(id) != null){
            return bookDAO.update(id, book);
        }
        return 0;
    }
    public void deleteById(Integer id){
        if (bookDAO.show(id) != null){
            bookDAO.delete(id);
        }
    }

}
