package ru.chernyavskiy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.chernyavskiy.dao.GenreDAO;
import ru.chernyavskiy.models.Genre;

import java.util.List;

@Service
public class GenreService {
    private final GenreDAO genreDAO;

    @Autowired
    public GenreService(GenreDAO genreDAO) {
        this.genreDAO = genreDAO;
    }

    public List<Genre> getAllGenre(){
        return genreDAO.index();
    }

    public Genre getGenreById(Integer id){
        return genreDAO.show(id).orElse(null);
    }

    public int save(Genre genre){
        return  genreDAO.save(genre);
    }

    public int editById(Integer id, Genre genre){
        if (genreDAO.show(id) != null){
            return genreDAO.update(id, genre);
        }
        return 0;
    }

    public void deleteById(Integer id){
        if (genreDAO.show(id) != null){
            genreDAO.delete(id);
        }
    }

}
